# AngularJS + Cordova + Angular Material App

This is an example Cordova app that uses angularjs and angular material.

# Dependencies
  - Cordova
  - NodeJs
  - npm
  - gulp
  

# Getting started
Clone the repository
```sh
$ git clone git@gitlab.com:FranGATV/cordova-angularjs-material.git
```
install npm dependencies
```sh
$ npm install
```
install bower dependecies
```sh
$ bower install
```
Develop in local server 
```sh
$ gulp
```
Run  in cordova (Ej: Android)
```sh
$ cordova platform add android
$ cordova run android
```
